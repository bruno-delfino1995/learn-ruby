require 'category'
require 'subcategory'

# Code without the RSpec DSL to understand the 'magic' behind the matcher, it's a
# contract following the ducky typing, the method to of RSpec only executes the
# matches? of an object and receives a boolean response indicating the match
#
# class ProductsMatcher
#   def initialize(*expected)
#     @products = expected
#     @products.flatten!
#   end
#
#   def matches?(category)
#     @category = category
#
#     subcategories_products = category.subcategories.map do |sub|
#       sub.products
#     end
#     subcategories_products.flatten!
#
#     subcategories_products == @products
#   end
#
#   def failure_message
#     "expected category #{@category.name} to contain products #{@products}"
#   end
# end
#
# def contain_products(*products)
#   ProductsMatcher.new products
# end

RSpec::Matchers.define :contain_products do |*products|
  match do |category|
    subcategories_products = category.subcategories.map do |sub|
      sub.products
    end
    subcategories_products.flatten!

    expect(subcategories_products).to eq(products)
  end

  failure_message do |category|
    "expected category #{category.name} to contain products #{products}"
  end
end

describe Category do
  it 'contains all the products of its subcategories' do
    eletronics = Category.new 'eletronics'
    computers = Subcategory.new 'computers'
    cell_phones = Subcategory.new 'cell-phones'

    computers.add_product 'MacBook'
    cell_phones.add_product 'iPhone'

    eletronics.add_subcategories computers, cell_phones

    expect(eletronics).to contain_products('MacBook', 'iPhone')
  end
end
