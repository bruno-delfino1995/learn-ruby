module Publishable
  attr_reader :published_on

  def publish!
    @published_on = Time.now.strftime('%Y-%m-%d')
  end
end
