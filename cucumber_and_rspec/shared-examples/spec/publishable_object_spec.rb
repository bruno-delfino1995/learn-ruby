require 'publishable_object'
require 'shared_examples'

describe 'A published object' do
  # subject can be used without the name, so access the object we just call subject
  subject do PublishableObject.new end

  # include the examples defined in some context with the specified name, as the
  # modules, the shared examples have access to the variables of where it was included,
  # so sometimes you need to have a specific variable to the shared test work. In
  # this case the variable/method is subject, because it was not declared in the example
  include_examples 'a publishable object'
end
