require 'blog_post'
require 'shared_examples'

describe BlogPost do
  # We can use the method it_behaves_like to specify that the current context behaves
  # like the included examples
  # Note that we've declared the subject, it's because the method describe when used
  # with a class automatically defines an subject with the class as value
  it_behaves_like 'a publishable object'
end
