shared_examples_for 'a publishable object' do
  describe '#publish' do
    it 'saves the publication date' do
      subject.publish!

      today = Time.now.strftime('%Y-%m-%d')
      expect(subject.published_on).to eq(today)
    end
  end
end
