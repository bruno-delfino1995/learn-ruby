class Game
  attr_reader :output, :score
  attr_accessor :phase

  def initialize
    @output = ''
    @phase = :initial
    @score = 0
  end

  def player_hits_target
    if phase == :final
      @score = 100
      @output = 'Congratulations!'
    else
      @score += 1
    end

    if @score > 33 then @phase = :middle
    elsif @score > 66 then @phase = :final
    end
  end
end
