require_relative 'cli_ui'
require_relative 'game'
require 'forwardable'

class GameFlow
  extend Forwardable
  delegate ended?: :@game

  def initialize(game = Game.new, ui = CliUi.new)
    @game = game
    @ui = ui
  end

  def start
    @ui.write 'Bem vindo ao jogo da forca!'
  end

  def next_step
    case @game.state
    when :initial
      ask_to_raffle_a_word
    when :word_raffled
      ask_to_guess_a_letter
    else
      print_game_final_result
    end
  end

  private

  def print_game_final_result
    if @game.player_won?
      @ui.write 'Você venceu! :)'
    else
      @ui.write 'Você perdeu. :('
    end
  end

  def ask_to_raffle_a_word
    ask_the_player 'Qual o tamanho da palavra a ser sorteada?' do |input|
      if @game.raffle(input.to_i)
        @ui.write guessed_letters
      else
        error_message = 'Não temos uma palavra com o tamanho desejado, é ' <<
          'necessário escolher outro tamanho.'
        @ui.write error_message
      end
    end
  end

  def ask_to_guess_a_letter
    ask_the_player 'Qual letra você acha que a palavra tem?' do |letter|
      if @game.guess_letter(letter)
        @ui.write 'Você adivinhou uma letra com sucesso.'
        @ui.write guessed_letters
      else
        @ui.write 'Você errou a letra.'
        @ui.write 'O boneco da forca perdeu as seguintes partes do corpo: ' <<
          @game.missed_parts.join(', ')
      end
    end
  end

  def ask_the_player(question)
    @ui.write question
    input = @ui.read.strip

    if input == 'fim'
      @game.finish
    else
      yield input
    end
  end

  def guessed_letters
    letters = ''
    guessed_letters = @game.guessed_letters

    @game.raffled_word.each_char do |letter|
      if guessed_letters.include?(letter)
        letters << letter + ' '
      else
        letters << '_ '
      end
    end

    letters.strip
  end
end
