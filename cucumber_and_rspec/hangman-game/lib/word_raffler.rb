# Raffle the words from a given list
class WordRaffler
  def initialize(words = %w[hi mom game fruit])
    @words = words
  end

  def raffle(length)
    @words.detect do |word|
      word.length == length
    end
  end
end
