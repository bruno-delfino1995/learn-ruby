# We are using require_relative instead of require because when we use require,
# it will search through directories in $LOAD_PATH. You should use require_relative
# when the file you need to load exists somewhere relative to the file that calls
# for the loading. Reserve require for "external" dependencies. But it's not a strict
# rule, it's just a recommendation, because when you build a gem you use require
# passing 'gemname/file'. It's work because the RubyGems puts your gem inside $GEM_PATH
# and override require for search in that directories, based on gemnames
require_relative 'word_raffler'

class Game
  attr_reader :raffled_word, :state, :guessed_letters, :missed_parts

  HANGMAN_PARTS = [
    'cabeça', 'corpo', 'braço esquerdo', 'braço direito', 'perna esquerda',
    'perna direita'
  ]

  def initialize(raffler = WordRaffler.new)
    @raffler = raffler
    @state = :initial
    @guessed_letters = []
    @missed_parts = []
  end

  def ended?
    @state == :ended
  end

  def raffle(word_length)
    if @raffled_word = @raffler.raffle(word_length)
      @state = :word_raffled
    end
  end

  def guess_letter(letter)
    return false if letter == ''

    if @raffled_word.include? letter
      if @guessed_letters.include? letter
        add_a_new_missed_part
        return false
      else
        @guessed_letters << letter
        @state = :ended if all_letters_were_guessed?

        return true
      end
    else
      add_a_new_missed_part
      return false
    end
  end

  def player_won?
    return false if @state != :ended

    all_letters_were_guessed?
  end

  def finish
    @state = :ended
  end

  private

  def all_letters_were_guessed?
    raffled_word_letters = @raffled_word.to_s.chars.to_a.uniq.sort
    @guessed_letters.sort == raffled_word_letters
  end

  def add_a_new_missed_part
    @missed_parts << HANGMAN_PARTS[@missed_parts.length]
    @state = :ended if @missed_parts.length == 6
  end
end
