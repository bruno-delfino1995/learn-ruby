Feature: Adivinhar letra
  Após a palavra do jogo ser sorteada, o jogador pode começar a tentar adivinhar
  as letras da palabra.

  Cada vez que ele acerta uma letra, o jogo mostra para ele em que posição dentro
  da palavra está a letra que ele acertou.

  Cada vez que o jogador erra uma letra, uma parte do boneco da forca aparece na
  forca. O jogador pode errar no máximo seis vezes, que correspondem às seis partes
  do boneco: cabeça, corpo, braço esquerdo, braço direito, perna esquerda e perna
  direita.

  # Here we define a background because when we read the following scenario, isn't
  # explicit why when the user type "a" the game count as a success hit. It's an
  # implicit thing in the test, an usual test code smell. The background is defined
  # to understand the cause and consequence of the test more clearly
  Background:
    * o jogo tem as possíveis palavras para sortear:
      | número de letras | palavra sorteada |
      | 3                | avo              |

  # In addition to the feature description, we need to specify the acceptance criterias.
  # To define it, we can write some examples about how is the behavior of this feature
  # in specific scenarios
  Scenario: Sucesso ao adivinhar letra
    Se o jogador adivinhar a letra com sucesso, o jogo mostra uma mensagem de
    sucesso e mostra em que posição está a letra que o jogador adivinhou.

    Given que comecei um jogo
    And que escolhi que a palavra a ser sorteada deverá ter "3" letras
    When tento adivinhar que a palavra tem a letra "a"
    And termino o jogo
    Then o jogo mostra que adivinhei uma letra com sucesso
    And o jogo termina com a seguinte mensagem na tela:
      """
      a _ _
      """

  Scenario: Erro ao adivinhar letra
    Se o jogador errar ao tentar adivinhar a letra, o jogo mostra uma mensagem de
    erro e mostra quais as parted o boneco da forca já perdeu.

    Given que comecei um jogo
    And que escolhi que a palavra a ser sorteada deverá ter "3" letras
    When tento adivinhar que a palavra tem a letra "z"
    And termino o jogo
    Then o jogo mostra errei a adivinhação da letra
    And o jogo termina com a seguinte mensagem na tela:
      """
      O boneco da forca perdeu as seguintes partes do corpo: cabeça
      """

  Scenario: Jogador adivinha com sucesso duas vezes
    Quanto mais o jogador for acertando, mais o jogo vai mostrando para ele as letras
    que ele adivinhou.

    Given que comecei um jogo
    And que escolhi que a palavra a ser sorteada deverá ter "3" letras
    When tento adivinhar que a palavra tem a letra "a"
    And tento adivinhar que a palavra tem a letra "v"
    And termino o jogo
    And o jogo termina com a seguinte mensagem na tela:
      """
      a v _
      """

  Scenario: Jogador erra três vezes ao adivinhar letra
    Quanto mais o jogador for errando, mais partes do boneco da forca são perdidas.

    Given que comecei um jogo
    And que escolhi que a palavra a ser sorteada deverá ter "3" letras
    When tento adivinhar que a palavra tem a letra "z"
    And tento adivinhar que a palavra tem a letra "y"
    And termino o jogo
    And o jogo termina com a seguinte mensagem na tela:
      """
      O boneco da forca perdeu as seguintes partes do corpo: cabeça, corpo
      """
