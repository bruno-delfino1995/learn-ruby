require 'game'

describe Game do
  # We will use a new class to raffle the word, we've arrived at this solution because
  # the private method raffle_word is too much important to don't have tests.
  # If you feel the necessity of test something, it's a signal that you should
  # make that thing testable
  let :raffler do
    raffler = double 'raffler'
    allow(raffler).to receive(:raffle)

    raffler
  end

  subject :game do
    # We have to specify our dependencies clearly to avoid a strong coupling and
    # improve the testability. It's because if we had used the method puts, we
    # would not have been able to test the output because we cannot read from STDOUT
    Game.new raffler
  end

  describe '#ended?' do
    it 'returns false when the game just started' do
      expect(game).to_not be_ended
    end
  end

  describe '#raffle' do
    it 'raffles a word with the given length' do
      expect(raffler).to receive(:raffle).with(3)

      game.raffle 3
    end

    it 'saves the raffled word' do
      expect(raffler).to receive(:raffle).and_return('mom')

      game.raffle 3

      expect(game.raffled_word).to eq('mom')
    end

    it 'makes a transition from :initial to :word_raffled on success' do
      expect(raffler).to receive(:raffle).and_return('word')

      expect do
        game.raffle 4
      end.to change { game.state }.from(:initial).to(:word_raffled)
    end

    it 'stays on the :initial state when a word can\'t be raffled' do
      expect(raffler).to receive(:raffle).and_return(nil)

      game.raffle 3

      expect(game.state).to eq(:initial)
    end
  end

  describe '#finish' do
    it 'sets the game as ended' do
      game.finish

      expect(game).to be_ended
    end
  end

  describe '#guess_letter' do
    before do
      expect(raffler).to receive(:raffle).and_return('hey')
      game.raffle 3
    end

    it 'returns true if the raffled word contains the given letter' do
      expect(game.guess_letter('e')).to eq(true)
    end

    it 'returns false if the raffled word contains the doesn\'t given letter' do
      expect(game.guess_letter('z')).to eq(false)
    end

    it 'returns false if the given letter is a blank string' do
      expect(game.guess_letter('')).to eq(false)
      expect(game.guess_letter('   ')).to eq(false)
    end

    it 'saves the guessed letter when the guess is right' do
      expect do
        game.guess_letter 'h'
      end.to change { game.guessed_letters }.from([]).to(['h'])
    end

    it 'does not save a guessed letter more than once' do
      game.guess_letter 'h'

      expect do
        game.guess_letter 'h'
      end.to_not change { game.guessed_letters }.from(['h'])
    end

    it 'returns false if the given letter was already guessed' do
      game.guess_letter 'h'

      expect(game.guess_letter('h')).to eq(false)
    end

    it 'updates the missed parts when the guess is wrong' do
      game.guess_letter 'z'

      expect(game.missed_parts).to eq(['cabeça'])
    end

    it 'updates the missed parts when the guess is right, but repeated' do
      game.guess_letter 'h'
      game.guess_letter 'h'

      expect(game.missed_parts).to eq(['cabeça'])
    end

    it 'makes a transition to the \'ended\' state when all the letters are guessed' <<
      'with success' do
      expect do
        game.guess_letter 'h'
        game.guess_letter 'e'
        game.guess_letter 'y'
      end.to change { game.state }.from(:word_raffled).to(:ended)
    end

    it 'makes a transaction to the \'ended\' state when the player miss 6 times ' <<
      'trying to guess the letter' do
        expect do
          6.times { game.guess_letter 'z' }
        end.to change { game.state }.from(:word_raffled).to(:ended)
      end
  end

  describe '#guessed_letters' do
    it 'returns the guessed letters' do
      expect(raffler).to receive(:raffle).and_return('hey')
      game.raffle 3
      game.guess_letter 'e'

      expect(game.guessed_letters).to eq(['e'])
    end

    it 'returns an empty array when there\'s no guessed letters' do
      expect(game.guessed_letters).to eq([])
    end
  end

  describe '#missed_parts' do
    it 'returns an empty array when there\'s no missed parts' do
      expect(game.missed_parts).to eq([])
    end

    it 'returns the missed parts for each fail in guessing a letter' do
      expect(raffler).to receive(:raffle).and_return('hey')
      game.raffle 3

      3.times do
        game.guess_letter 'z'
      end

      expect(game.missed_parts).to eq(['cabeça', 'corpo', 'braço esquerdo'])
    end
  end

  describe '#player_won?' do
    it 'returns true when the player guessed all letters with success' do
      expect(raffler).to receive(:raffle).and_return('hi')
      game.raffle 2

      game.guess_letter 'h'
      game.guess_letter 'i'

      expect(game.player_won?).to eq(true)
    end

    it 'returns false if the game isn\'t ended' do
      expect(game.player_won?).to eq(false)

      expect(raffler).to receive(:raffle).and_return('hi')
      game.raffle 2

      expect(game.player_won?).to eq(false)
    end

    # When a test pass in the first attempt check if the test is passing because
    # the corresponding code is implemented, check if it's passing because the right
    # reason. The normal flow in TDD is red-green-refactor, so when you faces with a
    # green in the fist step make some checks
    it 'returns false when the player didn\'t guessed all letters' do
      expect(raffler).to receive(:raffle).and_return('hi')
      game.raffle 2

      6.times do game.guess_letter 'z' end

      expect(game.player_won?).to  eq(false)
    end
  end

  context 'when just created' do
    it 'the state should be :initial' do
      expect(game.state).to eq(:initial)
    end
  end
end
