require 'game_flow'
require 'rspec/collection_matchers'

describe GameFlow do
  # We use a let to define an helper method that returns a RSpec mock that can
  # receive a call to the specified methods and execute the code inside the block provided
  # to receive. Thus, we also define a collaborative object to be used by the SUT
  let :ui do
    ui = double 'ui'
    allow(ui).to receive(:read).and_return('')
    allow(ui).to receive(:write)

    ui
  end

  let :game do
    game = double 'game'
    allow(game).to receive(:state).and_return(:initial)
    allow(game).to receive(:raffled_word).and_return('hey')
    allow(game).to receive(:raffle)
    allow(game).to receive(:guess_letter)
    allow(game).to receive(:guessed_letters).and_return([])
    allow(game).to receive(:missed_parts).and_return([])
    allow(game).to receive(:player_won?).and_return(false)
    allow(game).to receive(:finish)

    game
  end

  subject :game_flow do
    GameFlow.new game, ui
  end

  describe '#start' do
    it 'prints the initial message' do
      expect(ui).to receive(:write).with('Bem vindo ao jogo da forca!')

      game_flow.start
    end
  end

  describe '#next_step' do
    context 'when te game is in the \'initial\' state' do
      it 'asks the player for the length of the word to be raffled' do
        expect(ui).to receive(:read).and_return('3')
        expect(ui).to receive(:write).with('Qual o tamanho da palavra a ser sorteada?')

        game_flow.next_step
      end

      context 'and the player asks to raffle a word' do
        it 'raffles a word with the given length' do
          expect(ui).to receive(:read).and_return('3')
          expect(game).to receive(:raffle).with(3)

          game_flow.next_step
        end

        it 'prints a \'_\' for each letter in the raggled word' do
          expect(ui).to receive(:read).and_return('3')
          expect(game).to receive(:raffle).with(3).and_return('mom')
          expect(game).to receive(:raffled_word).and_return('mom')
          expect(ui).to receive(:write).with('_ _ _')

          game_flow.next_step
        end

        # Using mock objects before implementing some concrete class we focus on the
        # messages contract which that class will accept, we focus on the API, before
        # focusing on the implementation
        it 'tells if it\'s not possible to raffle with the given length' do
          expect(ui).to receive(:read).and_return('20')
          expect(game).to receive(:raffle).with(20).and_return(nil)
          error_message = "Não temos uma palavra com o tamanho desejado, é " <<
            "necessário escolher outro tamanho."
          expect(ui).to receive(:write).with(error_message)

          game_flow.next_step
        end
      end
    end

    context 'when the game is in the \'word raffled\' state' do
      before do
        expect(game).to receive(:state).and_return(:word_raffled)
      end

      it 'asks the player to guess a letter' do
        expect(ui).to receive(:write).with('Qual letra você acha que a palavra tem?')

        game_flow.next_step
      end

      context 'and the player guess a letter with success' do
        before do
          expect(game).to receive(:guess_letter).and_return(true)
        end

        it 'prints a success message' do
          expect(ui).to receive(:write).with('Você adivinhou uma letra com sucesso.')

          game_flow.next_step
        end

        it 'prints the guessed letters' do
          expect(game).to receive(:raffled_word).and_return('hey')
          expect(game).to receive(:guessed_letters).and_return(['e'])
          expect(ui).to receive(:write).with('_ e _')

          game_flow.next_step
        end
      end

      context 'and the player fails to guess a letter' do
        before do
          expect(game).to receive(:guess_letter).and_return(false)
        end

        it 'prints a error message' do
          expect(ui).to receive(:write).with('Você errou a letra.')

          game_flow.next_step
        end

        it 'prints the list of missed parts' do
          missed_parts_message = 'O boneco da forca perdeu as seguintes partes ' <<
            'do corpo: cabeça'
          expect(ui).to receive(:write).with(missed_parts_message)
          expect(game).to receive(:missed_parts).and_return(['cabeça'])

          game_flow.next_step
        end
      end
    end

    context 'when the game is in the \'ended\' state' do
      before do
        expect(game).to receive(:state).and_return(:ended)
      end

      it 'prints a success message when the player win' do
        expect(game).to receive(:player_won?).and_return(true)
        expect(ui).to receive(:write).with('Você venceu! :)')

        game_flow.next_step
      end

      it 'prints a defeat message when the player lose' do
        expect(game).to receive(:player_won?).and_return(false)
        expect(ui).to receive(:write).with("Você perdeu. :(")

        game_flow.next_step
      end
    end

    # After moving the test that was from game, we stop testing the object game,
    # for example, we don't test if the game is ended, it's because the new SUT
    # is game_flow
    it 'finished the game when the player asks to' do
      expect(ui).to receive(:read).twice.and_return('fim')

      expect(game).to receive(:state).and_return(:initial)
      expect(game).to receive(:finish)
      game_flow.next_step

      expect(game).to receive(:state).and_return(:word_raffled)
      expect(game).to receive(:finish)
      game_flow.next_step
    end
  end
end
