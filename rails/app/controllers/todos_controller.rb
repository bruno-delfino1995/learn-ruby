class TodosController < ApplicationController
  def index
    @todos = Todo.all
  end

  def new
  end

  def create
    @todo = Todo.new create_params
    @todo.save!
  end

  def show
    @todo = Todo.find find_params
  end

  def edit
    @todo = Todo.find find_params
  end

  def update
    @todo = Todo.find find_params
    @todo.update!(update_params)
  end

  def destroy
    @todo = Todo.find find_params
  end

  private

  def create_params
    params.require(:todo).permit(:name, :date, :description)
  end

  def find_params
    params.require(:id)
  end

  def update_params
    params.permit(:name, :date, :description)
  end
end
